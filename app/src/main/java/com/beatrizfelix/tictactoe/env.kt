package com.beatrizfelix.tictactoe

const val DB_NAME = "PDM_SQLite"
const val DB_VERSION = 1

const val TB_USER = "user"
const val TB_USER_ID =  "user_id"
const val TB_USER_USERNAME = "username"
const val TB_USER_PASSWORD = "password"

const val TB_GAME = "game"
const val TB_GAME_ID =  "game_id"
const val TB_GAME_PLAYER1 =  "player1"
const val TB_GAME_PLAYER2 =  "player2"
const val TB_GAME_WINNER =  "winner"

const val TB_USER_GAME = "user_game"
const val TB_USER_GAME_ID =  "user_game_id"
const val TB_USER_LOGGED =  "user_logged_id"
const val TB_USER_GAME_COUNT = "rounds"