package com.beatrizfelix.tictactoe

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper


class SqlHelper(context: Context) : SQLiteOpenHelper(context, DB_NAME, null, DB_VERSION) {

    override fun onCreate(sqLiteDatabase: SQLiteDatabase) {
        sqLiteDatabase.execSQL(
            "CREATE TABLE $TB_USER (" +
                    "$TB_USER_ID INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    "$TB_USER_USERNAME TEXT NOT NULL UNIQUE, " +
                    "$TB_USER_PASSWORD TEXT NOT NULL)"
        )
        sqLiteDatabase.execSQL(
            "CREATE TABLE $TB_USER_GAME (" +
                    "$TB_USER_GAME_ID INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    "$TB_USER_LOGGED INTEGER NOT NULL, " +
                    "$TB_USER_GAME_COUNT INTEGER DEFAULT 3, FOREIGN KEY($TB_USER_LOGGED) REFERENCES $TB_USER($TB_USER_ID) )"
        )
        sqLiteDatabase.execSQL(
            "CREATE TABLE $TB_GAME (" +
                    "$TB_GAME_ID INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    "$TB_GAME_PLAYER1 TEXT NOT NULL, " +
                    "$TB_GAME_PLAYER2 TEXT NOT NULL, " +
                    "$TB_GAME_WINNER TEXT NOT NULL)"
        )
    }

    override fun onUpgrade(sqLiteDatabase: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

}

