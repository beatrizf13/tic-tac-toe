package com.beatrizfelix.tictactoe

import android.content.ContentValues
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.StrictMode
import android.view.View
import android.widget.Toast

class PaymentActivity : AppCompatActivity() {

    private var player : String? = ""
    private var userLogged : Int? = 0

    private var chips : Int? = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payment)

        player = intent.getStringExtra("player")
        userLogged = intent.getIntExtra("userLogged", 0)

        chips = intent.getIntExtra("chips", 0)

        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)
    }

    fun pay (view: View) {
        addChips()

        Toast.makeText(this@PaymentActivity, "Payment successful", Toast.LENGTH_LONG).show()

        val intent = Intent(this, MainActivity::class.java)
        intent.putExtra("chips", chips!!)
        startActivity(intent)
        finish()
    }

    private fun addChips() {
        val database = SqlHelper(this).writableDatabase

        val newChips = chips?.plus(3)
        chips = newChips

        val contentValues = ContentValues().apply {
            put(TB_USER_GAME_COUNT, newChips)
        }

        database.update(
            TB_USER_GAME, contentValues, "$TB_USER_LOGGED = ?", arrayOf(userLogged.toString())
        )

        database.close()
    }

    fun backToMain (view: View) {
        val intent = Intent(this, MainActivity::class.java)

        intent.putExtra("player", player)
        intent.putExtra("userLogged", userLogged)
        intent.putExtra("chips", chips)

        startActivity(intent)
        finish()
    }
}