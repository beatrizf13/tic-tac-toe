package com.beatrizfelix.tictactoe

import android.content.ContentValues
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        loginRequestBtn.setOnClickListener {
            val chips: Int?
            val id: Int?

            val username: String? = inputUsername.text.toString()
            val password: String? = inputPassword.text.toString()


            val database = SqlHelper(this).writableDatabase

            val sql =
                "SELECT * FROM $TB_USER JOIN $TB_USER_GAME ON $TB_USER.$TB_USER_ID = $TB_USER_GAME.$TB_USER_LOGGED" +
                        " WHERE $TB_USER.$TB_USER_USERNAME = ? AND" +
                        " $TB_USER.$TB_USER_PASSWORD = ?"

            val cursor = database.rawQuery(sql, arrayOf(username, password))

            if (cursor.moveToFirst()) {
                id = cursor.getInt(cursor.getColumnIndex(TB_USER_ID))
                chips = cursor.getInt(cursor.getColumnIndex(TB_USER_GAME_COUNT))

                val intent = Intent(this, MainActivity::class.java)
                intent.putExtra("player", cursor.getString(cursor.getColumnIndex(TB_USER_USERNAME)))
                intent.putExtra("playerId", id)
                intent.putExtra("chips", chips)

                cursor.close()
                database.close()

                startActivity(intent)

                finish()
            } else {
                if (username == "" || password == "") {
                    Toast.makeText(
                        this,
                        "fill all the fields",
                        Toast.LENGTH_LONG
                    ).show()
                } else {
                    val contentValues = ContentValues().apply {
                        put(TB_USER_USERNAME, username)
                        put(TB_USER_PASSWORD, password)
                    }

                    val newUserId = database.insert(
                        TB_USER, null, contentValues
                    )

                    val contentValuesLogin = ContentValues().apply {
                        put(TB_USER_LOGGED, newUserId)
                        put(TB_USER_GAME_COUNT, 3)

                    }

                    database.insert(
                        TB_USER_GAME, null, contentValuesLogin
                    )

                    if (newUserId != -1L) {
                        Toast.makeText(
                            this,
                            "user successfully registered",
                            Toast.LENGTH_LONG
                        ).show()

                        loginRequestBtn.performClick()
                    } else {
                        Toast.makeText(
                            this,
                            "wrong password",
                            Toast.LENGTH_LONG
                        ).show()
                    }

                    database.close()
                }
            }
        }
    }
}


