package com.beatrizfelix.tictactoe

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private var userLogged: Int? = 0
    private var chips: Int? = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        chips = intent.getIntExtra("chips", 0)
        chipsText.text = "You have $chips chips"

        player1text.setText(intent.getStringExtra("player"))
        userLogged = intent.getIntExtra("userLogged", 0)

        if (intent.getIntExtra("chips", 0) < 1) {
            Toast.makeText(
                this,
                "Expired chips. Buy more to play!",
                Toast.LENGTH_LONG
            ).show()
        }
    }

    fun exit(view: View) {
        val intent = Intent(this, LoginActivity::class.java)
        startActivity(intent)
        finish()
    }

    fun startGame(view: View) {
        val buttonClicked = view as Button

        when (buttonClicked.id) {
            R.id.playRequestBtn -> {
                val player1 = player1text.text.toString()
                val player2 = player2text.text.toString()

                if (player1.trim() == "" || player2.trim() == "") {
                    Toast.makeText(
                        this,
                        "enter player names",
                        Toast.LENGTH_LONG
                    ).show()
                } else {
                    if (chips!! > 0) {
                        val intent = Intent(this, GameActivity::class.java)
                        intent.putExtra("player1", player1)
                        intent.putExtra("player2", player2)
                        intent.putExtra("userLogged", userLogged)
                        intent.putExtra("chips", chips!!)
                        startActivity(intent)
                        finish()
                    }
                }
            }

            R.id.historyBtn -> {
                val intent = Intent(this, HistoryActivity::class.java)
                intent.putExtra("player", player1text.text.toString())
                intent.putExtra("userLogged", userLogged)
                intent.putExtra("chips", chips!!)
                startActivity(intent)
                finish()
            }

            R.id.buyRequestBtn -> {
                val intent = Intent(this, PaymentActivity::class.java)
                intent.putExtra("player", player1text.text.toString())
                intent.putExtra("userLogged", userLogged)
                intent.putExtra("chips", chips!!)
                startActivity(intent)
                finish()
            }
        }
    }


}
