package com.beatrizfelix.tictactoe

import android.content.Intent
import android.database.sqlite.SQLiteException
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_history.*

class HistoryActivity : AppCompatActivity() {

    private var player : String? = ""
    private var userLogged : Int? = 0

    private var chips : Int? = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_history)

        player = intent.getStringExtra("player")
        userLogged = intent.getIntExtra("playerId", 0)
        chips = intent.getIntExtra("chips", 0)

        try {
            val datebase = SqlHelper(this).writableDatabase
            val cursor = datebase.rawQuery(
                "SELECT * FROM $TB_GAME",
                null
            )

            while (cursor.moveToNext()) {
                val text = cursor.getString(cursor.getColumnIndex(TB_GAME_PLAYER1)) + " vs " +
                        cursor.getString(cursor.getColumnIndex(TB_GAME_PLAYER2)) + " -> " +
                        cursor.getString(cursor.getColumnIndex(TB_GAME_WINNER)) + "\n"
                history.append(text)
            }

            cursor.close()
            datebase.close()
        } catch (e: SQLiteException) {
            Toast.makeText(this, e.toString(), Toast.LENGTH_LONG).show()
        }
    }

    fun backToMain(view: View){
        val intent = Intent(this, MainActivity::class.java)

        intent.putExtra("player", player)
        intent.putExtra("userLogged", userLogged)
        intent.putExtra("chips", chips)

        startActivity(intent)
        finish()
    }
}


